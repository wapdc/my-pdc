# My PDC

This project houses the home page and administrative pages for Washington Public Disclosure Commission's My PDC web site
(apollo). 

The basic application is a Vue 3 Quasar application. 

## Menu system

The menu system for mypdc is stored in our RDS postgres database. Each piece of navigation is stored in the menu_links table. 
Revision scripts for each project can add items to the home and administration pages by inserting links into the menu_links
table using the following fields: 

**menu** - Indicates which page the links should be placed on.  A menu of **filer** will put the link on the home page 
while a menu of **staff** will put it on the administration page. 

**category** - Indicates a grouping category for the link.  Usually this is the project name (e.g. Campaign finance), however 
it often is **Other** when the link is relevant to multiple projects. 

**title** - The human-readable title that describes the menu item. 

**abstract** - A few sentences that describe what people can do when they visit the link or menu item.  

**url** - Path to the menu item.  Apollo links should be root relative (i.e. begin with a /). 

**icon** - The name of the material design icon that is displayed next to the menu item (e.g. search) 

**action** - The single word that will appear on the navigation button when used (e.g. File, Go)

**permission** - Any drupal right may be used here to limit whether the menu is displayed for the currently logged in user. 

**count_function** - The function name that is to be used to determine how many items need work in the task menu.  See below.

## Example registration scripts 

```postgresql
-- Insert a generic link
insert into menu_links(menu, category, title, abstract, url, icon) 
    VALUES ('filer', 'Campaign Finance', 'Campaign Finance', 'Register candidates and PACS and file campaign finance reports.', '/campaigns', 'monetization_on'); 

-- Insert a staff link with a count function 
insert into menu_links(menu, category, title, abstract, url, count_function)
  values('staff', 'Campaign finance', 'Verify Registrations', 'Connect newly submitted registration to known candidates and filers.', '/campaigns/admin/', 'cf_count_unverified_registrations'); 
```

## Count functions

As indicated above count functions are used to add menu items to the **Todo** section of the administration or home page.  

The following example illustrates a function that uses the context menu in a simple way.  

```postgresql
create or replace function count_context_properties(p_context JSON) returns int language plpgsql as $$
  declare 
    v_count integer;     
  begin 
    SELECT count(1) into v_count from jsonb_each_text(cast(p_context as jsonb)) ; 
    return v_count; 
  end
$$
```

The context parameter in the function definition is completely optional. In other words, the count function defined could 
take 0 parameters, but should always return an integer. 

The following is an example of the context json that is sent to the function: 

```json
{
  "user": {
    "user_name": "david.metzler@pdc.wa.gov",
    "uid": "2",
    "realm": "apollo"
  },
  "roles": ["administrator", "authenticated user"],
  "permissions": ["access wapdc data", "enter wapdc data"]
}
```

