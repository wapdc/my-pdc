create table menu_links(
    link_id serial primary key,
    category text default 'Other',
    menu text,
    title text,
    abstract text,
    icon text,
    action text,
    url text,
    permission text,
    count_function text
);
-- Records copied from apollo menus.
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('filer', '/campaigns/committees', 'Campaign Finance', 'Register a new committee (C-1 and C-1pc forms) or manage your existing campaigns', 'monetization_on', 'Manage Campaigns', null);
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('filer', '/inside/customer-service/home', 'For PDC Staff', 'Administrative tools for PDC staff', 'settings', 'Go', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('filer', '/financial-affairs/my-submissions', 'Financial Affairs Disclosure', 'Disclose personal financial affairs (F-1 report)', 'star', 'File', null);
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/data-admin/jurisdiction', 'Jurisdiction Search', 'Find information on jurisdiction including officials list', 'location', 'Search', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/data-admin/people/search', 'Person Search', 'Search, create and edit people and candidates', 'search', 'Search', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/campaigns/committee-search', 'Committee Search', 'Search registered candidate, surplus and political committees', 'search', 'Search', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/financial-affairs/admin', 'Financial Affairs', 'Administrative Tasks related to Financial Affairs', 'settings', 'Go', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/data-admin', 'Other Business Processes', 'Officials Lists, Notification/Compliance Collections, Jurisdiction Management etc.', null, 'Go', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/campaigns/admin', 'Campaign Finance', 'Administrative Tasks related to campaign Finance', 'settings', 'Go', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('filer', '/my-pdc/staff', 'Administration', 'Administrative Tools for PDC Staff', 'settings', 'Go', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('filer', '/compliance/case-manager/case_menu', 'Compliance', 'Compliance case tracking', 'work', 'Go', 'access_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/admin/content', 'Site Content', 'Manage messages and news in the electronic filing site. ', 'info', 'Go', 'enter_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('filer', '/iexpenditures', 'Independent Expenditures', 'Disclose independent expenditures/electioneering communications (C-6 report)', null, 'File', null);
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('filer', '/accesshub/login', 'Lobbying', 'Register to lobby and report lobbying expenditures by lobbyists and their clients (L-1, L-2 and L-3 reports)', 'spatial_audio_off', 'File', null);
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('filer', '/lobbyist/public-agency', 'Public Agency Lobbying', '<ul><li>File public agency lobbying reports (L5)</li><li>Register as a new agency</li></ul>', null, 'File', null);
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/lmc/admin/unmatched-reports', 'Last Minute Contributions', 'Last minute contributions that need matching', 'search', 'Go', 'enter_wapdc_data');
INSERT INTO menu_links(menu, url, title, abstract, icon, action, permission) VALUES ('staff', '/lobbyist/admin/access-requests', 'Lobbying Access Requests', 'Access requests for linking lobbyist application (i.e. public agency, firm, employer)', null, 'Go', 'access_wapdc_data');
-- Set categories
update menu_links set category='Campaign finance' where url ilike '%committee%' or url ilike '%lmc%' or url ilike '%iexpenditures%' or url ilike '%campaign%';
update menu_links set category='Financial affairs' where url ilike '%financial-affairs%';
update menu_links set category='Lobbying' where url ilike '%lobbyist%' or url ilike '%accesshub%';
update menu_links set category='Other' where category is null;
-- Change permissions to match drupal convention
update menu_links set permission = replace(permission, '_' ,' ') where  permission is not null;
