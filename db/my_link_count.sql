CREATE OR REPLACE FUNCTION my_link_count(context JSON, count_function text) returns int language plpgsql as $$
  declare
      v_count int;
      v_function text;
      v_args int;
      v_sql text;

  begin
    -- Verify the the database function exists and determine number of parameters.
    select p.proname, p.pronargs into v_function, v_args from pg_proc p
      where p.proname = count_function;

    if v_function is not null then

        if v_args = 1  then
          v_sql = 'SELECT ' || v_function || '($1)';
          RAISE notice 'SQL: %', v_sql;
          EXECUTE v_sql INTO v_count using context;
        elseif v_args = 0 then
          EXECUTE 'SELECT ' || v_function || '()' INTO v_count;
        else
          RAISE 'Unsupported function signature';
        end if;
    end if;
    return v_count;
  end
$$