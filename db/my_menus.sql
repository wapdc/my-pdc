create or replace function my_menus(context json) returns json as
$$
    declare
      j_response jsonb;
      v_menu record;

    begin

      -- select all the entries from the menu table
      j_response = jsonb_build_object();
      FOR v_menu in select menu, json_agg(json_build_object('group', category, 'links', links) order by category) m
           from ( select
                      menu, category, json_agg(json_build_object(
                          'title', l.title,
                          'url', l.url,
                          'abstract', l.abstract,
                          'action', l.action,
                          'icon', l.icon,
                          'count', my_link_count(context, l.count_function)) order by title)  as links
                  from menu_links l
                  where l.permission is null
                    or l.permission in (select value from json_array_elements_text(context->'permissions'))
                    or 'administrator' in (select value from json_array_elements_text(context->'roles'))
                  group by l.category, l.menu
                ) v
           group by menu loop
        j_response = jsonb_set(j_response, cast('{'||v_menu.menu || '}' as text[]) ,v_menu.m::jsonb , true );
      end loop;
      return cast(j_response as json);
    end
$$ language plpgsql;
