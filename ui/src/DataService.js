import axios from 'axios'
import {Loading, Notify} from "quasar"
let service_host
let _apollo_url
const devMode = (location.host.search('localhost') >= 0) || (location.host.search('127.0.0.1') >= 0) ||
  (location.host.search('lndo.site') >=0)

if (devMode) {
  // Assume ports that start with 818 (e.g. 8180-8189 will connect to demo environment)
  // so that devs with no lando work.
  service_host = 'https://apollod8.lndo.site/'
  _apollo_url = 'https://apollod8.lndo.site'
} else {
  service_host = '/'
  _apollo_url = location.protocol + "//" + location.host
}

export const apollo_url = _apollo_url;
export const apollo = axios.create({
  baseURL: service_host,
  withCredentials: true
})
export class GatewayDataService {
  init(gatewayUrl, token) {
    this.url = gatewayUrl;
    this.gateway = axios.create({
      baseURL: gatewayUrl,
      headers: {
        "Authorization": token
      }
    })
  }

  async get(path, uiBlocking = true, config = {}) {
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.get(path, config)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }catch(error){
      Notify.create({
        position: "top",
        type: "negative",
        message: "Request was unsuccessful: " + error.message,
      })
      console.error(error.message)
      if (uiBlocking) {
        Loading.hide()
      }
      return null
    }
  }

  async post(path, param_data, uiBlocking = true){
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.post(path, param_data)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }
    catch (error) {
      const error_message = (error?.response?.data?.message) ? error.response.data.message : error.message
      Notify.create({
        position: 'top',
        type: 'negative',
        message: 'Request was unsuccessful: ' + error_message,
      })
      console.error(error.message)
      if (uiBlocking) {
        Loading.hide()
      }
      return {}
    }
  }

  async put(path, param_data, uiBlocking = true){
    if (uiBlocking) {
      Loading.show()
    }
    try {
      const { data } = await this.gateway.put(path, param_data)
      if (uiBlocking) {
        Loading.hide()
      }
      return data
    }
    catch (error) {
      const error_message = (error?.response?.data?.message) ? error.response.data.message : error.message
      Notify.create({
        position: 'top',
        type: 'negative',
        message: 'Request was unsuccessful: ' + error_message,
      })
      console.error(error.message)
      if (uiBlocking) {
        Loading.hide()
      }
      return {}
    }
  }

  async delete(path, uiBlocking = true) {
    if(uiBlocking){
      Loading.show()
    }
    let success = true
    try{
      await this.gateway.delete(path)
      Loading.hide();
    }catch (error){
      success = false
      Notify.create({
        position: 'top',
        type: 'negative',
        message: 'Request was unsuccessful: ' + error.message,
      })
      console.error(error.message)
      if(uiBlocking){
        Loading.hide()
      }
    }
    return success
  }

}
