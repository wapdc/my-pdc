/**
 * Application level information store.
 *
 * When using, be sure to await the useApplication() call so that the appInfo and user properties are set.
 */
import {apollo,GatewayDataService} from './DataService'
import {reactive} from "vue";


const user = reactive({})


export const useApplication = async function() {
  if (Object.keys(user).length === 0) {
    await refresh()
  }
  return {user, loadGroupedNavigation,  loadNavigation, hasAccessToContent}
}

async function refresh() {
  const {data} = await apollo.get('public/user-info')
  // If there is a failure, wait and try again
  if (data) {
    Object.assign(user, data)
    // Set the timeout to renew to half the expiration seconds.
    setTimeout(refresh, 120000)
  }
  else {
    setTimeout(refresh,  2000)
  }
}

function userHasRight(right) {
  return user && user.rights && user.rights[right]
}

function hasAccessToContent(content) {
    return content.limitTo?.(user) ??
      (content.field_access?.[0]?.value && userHasRight(content.field_access[0].value)) ??
      true;
}

async function loadGroupedNavigation(location) {
  const {data} = await apollo.get(`public/service/my-pdc/menus`, {params: {_format: "json"}})
  if (data && data[location]) {
    return data[location]
  }
  else {
    return null;
  }
}

async function loadNavigation(location){
  const groupedNavigation = await loadGroupedNavigation(location)
  let links = []
  if (groupedNavigation) {
    groupedNavigation.forEach((group) => {
      group.links.forEach((link) => {
        if (link.permission) {
          link.limitTo = () => {return userHasRight(link.permission)}
        }
        links.push(link);
      });
    })
  }
  return links
}
const gateways = {};
const devMode = (location.host.search('localhost') >= 0) || (location.host.search('127.0.0.1') >= 0) ||
  (location.host.search('lndo.site') >=0) || (location.host.search('ddev.site') >= 0)
export async function useGateway(gatewayName, devPort) {
  let gateway;
  if (!gateways[gatewayName]) {
    gateways[gatewayName] = new GatewayDataService()
    gateway = gateways[gatewayName]

    // Figure out the url from wapdc_settting
    let gatewayUrl
    if (user.settings && user.settings[gatewayName]) {
      gatewayUrl = user.settings[gatewayName]
    }
    else if (devMode) {
      gatewayUrl = 'http://127.0.0.1:' + devPort
    }
    else {
      throw new Error('Gateway ' + gatewayName + ' not configured')
    }

    await gateway.init(gatewayUrl, user?.auth?.token ?? "")
    return gateway
  }
  else {
    return gateways[gatewayName]
  }

}
