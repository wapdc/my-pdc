import * as authorizer from '@wapdc/common/aws-gateway'

/**
 * authorizer functions in pdc-common. Sam template parameters pass event, context, callback
 * default filer level authorization
 */
export const authorize = authorizer.authorize

/**
 * authorizer functions in pdc-common. Sam template parameters pass event, context, callback
 * admin level authorization
 */
export const authorizeAdmin = authorizer.authorizeAdmin
