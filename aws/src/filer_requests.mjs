import {getRestApi, waitForCompletion} from "@wapdc/common/rest-api";
import {userProcessor} from "./UserProcessor.mjs";
const api = getRestApi();



api.post('/update-user-info', async(req, res) => {
  const userdata = req.body
  await userProcessor.updateUserInfo(userdata);
  res.json( {success: true})
})


/**
 * AWS Lambda handler. Runs the API with the provided event and context.
 *
 * @param {object} event - The AWS Lambda event object.
 * @param {object} context - The AWS Lambda context object.
 * @returns {Promise} - The promise to return a response.
 */
export const lambdaHandler = async (event, context) => {
  const result =   api.run(event, context);
  await waitForCompletion()
  return result
}