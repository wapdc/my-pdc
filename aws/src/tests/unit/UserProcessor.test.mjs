import * as rds from "@wapdc/common/wapdc-db"
import env from 'dotenv'
import {expect, test, describe,beforeAll,afterAll} from '@jest/globals';

import {userProcessor} from "../../UserProcessor.mjs";


env.config()


beforeAll(async () => {
  await rds.connect();
  await rds.beginTransaction();
});

afterAll(async () => {
  await rds.rollback();
  await rds.close();
});


describe('Update User Profile Information', ()  => {
  const uid = 1;
  const realm = 'yourRealm';
  const originalName = 'Original Name';
  const updatedName = 'Updated Name';
  const email = 'test@example.com';
  const phone = '1234567890';
// Payload for updating user info
  const payload = {
    uid: uid,
    name: updatedName,
    email: email,
    phone: phone,
    realm: realm
  };
  test('Updates user info and contact details', async () => {

    // Set up initial state with original name and contact details
    await rds.execute(`INSERT INTO pdc_user(uid,realm,user_name,name) VALUES ($1, $2, $3,$4)`, [uid,realm,'uname',originalName]);
    await rds.execute(`INSERT INTO pdc_user_contact(uid, protocol, target, realm) VALUES ($1, 'email', $2, $3),($1, 'voice', $4, $3)`, [uid, 'originalEmail@example.com', realm, 'originalPhone']);

    // Call the function to test
    await userProcessor.updateUserInfo(payload)

    // Fetch the updated user info
    const userInfo = await rds.fetch(`SELECT name FROM pdc_user WHERE uid = $1 and realm = $2`, [uid,realm]);
    expect(userInfo).toBeDefined();
    expect(userInfo.name).toBe(updatedName);

    // Fetch the updated contact details and check them
    const contactInfo = await rds.query(`SELECT protocol, target FROM pdc_user_contact WHERE uid = $1 AND realm = $2`, [uid, realm]);
    expect(contactInfo).toBeDefined();
    expect(contactInfo.length).toBe(2);
    for (const contact of contactInfo) {
      if (contact.protocol === 'email') {
        expect(contact.target).toBe(email);
      } else if (contact.protocol === 'voice') {
        expect(contact.target).toBe(phone);
      }
    }
  });
  test('Updates userinfo and insert contact details', async () => {
    // Set up initial state with insert user without contact details and remove contact info
    await rds.execute(`INSERT INTO pdc_user(uid,realm,user_name,name) VALUES ($1, $2, $3,$4) ON CONFLICT (uid,realm) DO UPDATE SET name = 'original name'`, [uid,realm,'uname',originalName]);
    await rds.execute(`DELETE FROM pdc_user_contact WHERE uid = $1 and realm = $2 and wapdc.public.pdc_user_contact.target='voice'`, [uid,realm]);


    // Call the function to test
    await userProcessor.updateUserInfo(payload);

    // Fetch and verify the updated user info
    const userInfo = await rds.query(`SELECT name FROM pdc_user WHERE uid = $1 and realm = $2`, [uid,realm]);
    expect(userInfo[0]).toBeDefined();
    expect(userInfo[0].name).toBe(updatedName);

    // Fetch and verify the inserted contact details
    const contactInfo = await rds.query(`SELECT protocol, target FROM pdc_user_contact WHERE uid = $1 AND realm = $2 ORDER BY protocol`, [uid, realm]);
    expect(contactInfo).toBeDefined();
    expect(contactInfo.length).toBe(2);

    const emailInfo = contactInfo.find(info => info.protocol === 'email');
    const phoneInfo = contactInfo.find(info => info.protocol === 'voice');

    expect(emailInfo).toBeDefined();
    expect(emailInfo.target).toBe(email);
    expect(phoneInfo).toBeDefined();
    expect(phoneInfo.target).toBe(phone);
  });

});