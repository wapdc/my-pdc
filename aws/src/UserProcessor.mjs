import * as rds from "@wapdc/common/wapdc-db"

class UserProcessor {

  async updateUserInfo(payload) {
    const { name, phone, email, uid, realm} = payload;
    if (!uid) {
      throw new Error("No user id");
    }
    if (!realm) {
      throw new Error("Realm is undefined");
    }
    // Update the pdc_user table
    await rds.execute(`UPDATE pdc_user SET name = $1 WHERE uid = $2 and realm = $3`, [name, uid, realm]);

    // Define protocol-target pairs
    const contacts = [
      { protocol: 'email', target: email },
      { protocol: 'voice', target: phone },
    ];
    let res
    for (const { protocol, target } of contacts) {
      // First, check if the record exists
      const checkRes = await rds.query(`select count(*) from pdc_user_contact where uid = $1 and protocol = $2 and realm = $3`, [uid, protocol, realm]);
      if (checkRes[0].count>0) {
        // If the record exists, update it
        res = await rds.execute(`update pdc_user_contact set target = $1 where uid = $2 and protocol = $3 and realm = $4`, [target, uid, protocol, realm]);
      } else {
        // If the record does not exist, insert new one
        res = await rds.execute(`insert into pdc_user_contact (uid,protocol,target,realm) values ($1, $2, $3, $4)`, [uid, protocol, target, realm]);
      }
    }
    return res;
  }
}

const userProcessor = new UserProcessor()
export {userProcessor}