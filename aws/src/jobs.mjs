import * as wapdcDb from '@wapdc/common/wapdc-db'
/**
 * The payload for this output is expected to be the json that is
 * generated from the aws cloud formation describe-stacks command for the
 * deployed instance.
 *
 * @param event
 * @returns {Promise<void>}
 */
export const registerUrl = async(event) => {
  console.info("Registration event", event);
  if (!event && !event.Stacks) {
    throw Error("Invalid or missing event");
  }
  const stack = event.Stacks[0];
  if (!stack || !stack.Outputs) {
    throw Error("Cannot find stack output to extract url");
  }
  const urlOutput = stack.Outputs.find(o => o.OutputKey === "ApiGatewayUrl");
  const nameOutput = stack.Outputs.find(o => o.OutputKey === 'ApiGatewayName')

  if (!urlOutput || !nameOutput) {
    console.error('Missing registration info', stack.Outputs);
    throw Error("Cannot find name/url pair in stack output")
  }

  await wapdcDb.connect();
  await wapdcDb.setWapdcSetting(nameOutput.OutputValue, urlOutput.OutputValue);
  await wapdcDb.close();
}
