# MY Aws Lambdas

This aws project provides the REST web services that allow users to authorize with lambdas from other proejcts. 

### Testing JWT authorization locally

The sam local environment does not invoke the authorizers that are defined in the SAM template, so if you want to test
the JWT decoding process you need to do that by manually invoking the lambda.  Use events/authorize.json as event as an example,
but create a local copy and modify the JWT authorizer token in the payload.

```bash
sam local invoke AuthorizeFilerFunction --event local/authorize.json --env-vars local/env.json
sam local invoke AuthorizeAdminFunction --event local/authorize.json --env-vars local/env.json

```

