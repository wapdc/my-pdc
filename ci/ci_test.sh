#!/usr/bin/env bash
set -e
cd aws/

node --version
npm install

export PGHOST="$RDS_WAPDC_DEMO_PGHOST"
export PGPASSWORD="$RDS_WAPDC_DEMO_PASSWORD"
export PGDATABASE="wapdc"
export PGUSER="wapdc"
export PGPORT="5432"
export PGSSLMODE="no-verify"
export STAGE=dev
npm run test

echo 'Successfully ran tests'