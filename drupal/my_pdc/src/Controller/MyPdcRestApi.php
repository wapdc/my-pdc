<?php
namespace Drupal\my_pdc\Controller;

use Drupal\wapdc_core\Controller\CoreControllerBase;
use \Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WAPDC\Core\CoreDataService;

class MyPdcRestApi extends CoreControllerBase {

  public function menus(Request $request) {
    $this->prepareRequest($request);
    try {
      $dm = CoreDataService::service()->getDataManager();
      $context =  new \stdClass();
      $pdcUser = $this->getCurrentUser(true);
      $drupalUser = $this->currentUser();

      // load user object into context
      $context->user = new \stdClass();
      $context->user->user_name = $pdcUser->user_name;
      $context->user->uid = $pdcUser->uid;
      $context->user->realm = $pdcUser->realm;
      $context->roles = $drupalUser->getRoles();
      $permissions = [];

      // Load permissions into user context.
      $roles_permissions = user_role_permissions($context->roles);
      foreach ($roles_permissions as $p) {
        foreach ($p as $permission) {
          $permissions[] = $permission;
        }
      }
      $context->permissions = $permissions;

      $menu_json = $dm->db->fetchOne("select my_menus(cast (:json as JSON))", ['json' => json_encode($context)]);
      return new JsonResponse(json_decode($menu_json));
    }
    catch (Exception $e) {
      \Drupal::logger('my_pdc')->error($e->getMessage());
      $message = "An unknown error occurred.";
      if (empty($_ENV["PANTHEON_ENVIRONMENT"]) || $_ENV["PANTHEON_ENVIRONMENT"] === 'lando') $message = $e->getMessage();
      return new JsonResponse($message, 500 );
    }

  }

}